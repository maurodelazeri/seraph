package api

import (
	docker "github.com/fsouza/go-dockerclient"
	"github.com/maurodelazeri/seraph/deploy/network"
	"github.com/maurodelazeri/seraph/deploy/stack"
)

// RemoveContainerResult stores the config
type RemoveContainerResult struct {
	ContainerConfig stack.ContainerConfig `json:"container_config"`
	NetworkConfig   network.Config        `json:"network_config"`
	ResponseMessage string                `json:"response_message"`
}

// RemoveContainerEndpoint ...
func (result *RemoveContainerResult) RemoveContainerEndpoint(cli docker.Client) {
	result.RemoveStackContainer(cli)
	result.RemoveNetwork(cli)
}

// RemoveNetwork ...
func (result *RemoveContainerResult) RemoveNetwork(cli docker.Client) {
	err := network.RemoveNetwork(&cli, &result.NetworkConfig)
	if err != nil {
		result.ResponseMessage = err.Error()
		return
	}
}

// RemoveStackContainer ...
func (result *RemoveContainerResult) RemoveStackContainer(cli docker.Client) {
	err := stack.RemoveStackContainer(&cli, &result.ContainerConfig)
	if err != nil {
		result.ResponseMessage = err.Error()
		return
	}
}
