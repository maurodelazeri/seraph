package api

import (
	docker "github.com/fsouza/go-dockerclient"
	"github.com/maurodelazeri/seraph/deploy/network"
	"github.com/maurodelazeri/seraph/deploy/stack"
)

// CreateNewContainerResult is model for response container creation result
type CreateNewContainerResult struct {
	StackConfig     stack.ContainerConfig `json:"container_config"`
	NetworkConfig   network.Config        `json:"network_config"`
	ResponseMessage string                `json:"response_message"`
}

// CreateNewContainerEndpoint ...
func (result *CreateNewContainerResult) CreateNewContainerEndpoint(cli docker.Client) {
	//	result.CreateNetwork(cli)
	result.CreateStackContainer(cli)
}

// CreateNetwork ...
func (result *CreateNewContainerResult) CreateNetwork(cli docker.Client) {

	// Check if the network already exist, if not create one
	networks, err := cli.ListNetworks()
	if err != nil {
		result.ResponseMessage = err.Error()
		return
	}

	createNetwork := true

	for _, net := range networks {
		configNetName := &result.NetworkConfig.NetworkName
		if net.Name == *configNetName {
			createNetwork = false
			result.NetworkConfig.NetworkID = net.ID
		}
	}

	if createNetwork {
		networkResult, err := network.CreateNetwork(&cli, &result.NetworkConfig)
		if err != nil {
			result.ResponseMessage = err.Error()
			return
		}
		result.NetworkConfig.NetworkID = networkResult.ID
	}

}

// CreateStackContainer ...
func (result *CreateNewContainerResult) CreateStackContainer(cli docker.Client) {

	// Creating the container
	stackResult, err := stack.CreateStackContainer(&cli, &result.StackConfig, &result.NetworkConfig)
	if err != nil {
		result.ResponseMessage = err.Error()
		return
	}
	result.StackConfig.ContainerID = stackResult.ID
}
