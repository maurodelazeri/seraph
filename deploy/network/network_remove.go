package network

import docker "github.com/fsouza/go-dockerclient"

// RemoveNetwork remove a docker network
func RemoveNetwork(cli *docker.Client, config *Config) error {
	return cli.RemoveNetwork(config.NetworkName)
}
