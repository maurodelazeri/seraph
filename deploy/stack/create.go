package stack

import (
	"fmt"

	"github.com/fsouza/go-dockerclient"
	"github.com/maurodelazeri/seraph/deploy/network"
	"github.com/sirupsen/logrus"
)

// CreateStackContainer create a lasted nginx container and configuration from API endpoint
func CreateStackContainer(cli *docker.Client, config *ContainerConfig, netconfig *network.Config) (*docker.Container, error) {

	logrus.WithFields(logrus.Fields{
		"container_name": config.ContainerName,
		"repo_tag":       config.ImageName,
	}).Info("Starting docker container")

	//binds := []string{"/etc/hosts:/etc/hosts"}
	binds := []string{}
	// configure volumes if persistence is enabled for this container
	// if persistenceEnabled {
	// 	image, err := InspectImage(repoTag)
	// 	if err != nil {
	// 		return err
	// 	}
	// 	for volume, _ := range image.Config.Volumes {
	// 		mountPath := path.Join(persistenceDir, name, volume)
	// 		binds = append(binds, fmt.Sprintf("%s:%s", mountPath, volume))
	// 	}
	// }

	// convert portMapping map into the map[Port][]PortBinding that docker expects
	portBindings := network.PortMappingToPortBindings(config.PortMapping)

	// convert portMapping map into the map[Port]struct{} that docker expects
	exposedPorts := make(map[docker.Port]struct{})
	for _, internalPort := range config.PortMapping {
		exposedPorts[docker.Port(fmt.Sprintf("%d/tcp", internalPort))] = struct{}{}
		//exposedPorts[docker.Port(fmt.Sprintf("%d/udp", internalPort))] = struct{}{}
	}

	// networkEndpointSetting := make(map[string]*docker.EndpointConfig)
	// // Setting container network

	// networkEndpointSetting["stack_net"] = &docker.EndpointConfig{
	// 	Gateway:   netconfig.Gateway,
	// 	NetworkID: netconfig.NetworkID,
	// }

	imageConfig := &docker.Config{}
	imageConfig.Image = config.ImageName
	imageConfig.Env = config.Env
	imageConfig.ExposedPorts = exposedPorts
	imageConfig.Entrypoint = config.Entrypoint

	imageConfig.CPUShares = config.CPUShares
	imageConfig.CPUSet = config.CPUSet
	imageConfig.MemorySwap = config.MemorySwap
	imageConfig.MemoryReservation = config.MemoryReservation
	imageConfig.KernelMemory = config.KernelMemory

	config.HostConfig = docker.HostConfig{RestartPolicy: docker.RestartOnFailure(10), Binds: binds, PortBindings: portBindings}

	// Setting container data
	containerOptions := docker.CreateContainerOptions{
		Name:       config.ContainerName,
		Config:     imageConfig,
		HostConfig: &config.HostConfig,
		// NetworkingConfig: &docker.Config{
		// 	EndpointsConfig: networkEndpointSetting,
		// },
	}

	// Create Container
	return cli.CreateContainer(containerOptions)
}
