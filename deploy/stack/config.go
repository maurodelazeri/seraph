package stack

import (
	docker "github.com/fsouza/go-dockerclient"
	"github.com/maurodelazeri/seraph/deploy/network"
)

// ContainerConfig holds parameters to configuration stack container setting
type ContainerConfig struct {
	CustomerID        string            `json:"customer_id,omitempty"`
	ContainerID       string            `json:"container_id,omitempty"`
	ContainerName     string            `json:"container_name,omitempty"`
	ImageName         string            `json:"image_name,omitempty"`
	HostConfig        docker.HostConfig `json:"host_config,omitempty"`
	PortMapping       map[int]int       `json:"port_mapping,omitempty"`
	Env               []string          `json:"env,omitempty"`
	Entrypoint        []string          `json:"entrypoint,omitempty"`
	Memory            int64             `json:"Memory,omitempty"`
	MemorySwap        int64             `json:"MemorySwap,omitempty"`
	MemoryReservation int64             `json:"MemoryReservation,omitempty"`
	KernelMemory      int64             `json:"KernelMemory,omitempty"`
	CPUShares         int64             `json:"CpuShares,omitempty"`
	CPUSet            string            `json:"Cpuset,omitempty"`
}

// Stack interface for stack control implementation
type Stack interface {
	CreateStackContainer(cli *docker.Client, config *ContainerConfig, netconfig *network.Config) (*docker.Container, error)
	RemoveStackContainer(cli *docker.Client, config *ContainerConfig) error
	StartStack(cli *docker.Client, config *ContainerConfig) error
	StopStack(cli *docker.Client, config *ContainerConfig) error
}
