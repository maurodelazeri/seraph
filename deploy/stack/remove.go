package stack

import docker "github.com/fsouza/go-dockerclient"

// RemoveStackContainer remove a nginx container
func RemoveStackContainer(cli *docker.Client, config *ContainerConfig) error {

	// Setting container option
	var removeContainerOption = docker.RemoveContainerOptions{
		ID:            config.ContainerID,
		Force:         true,
		RemoveVolumes: true,
	}
	// Remove container
	return cli.RemoveContainer(removeContainerOption)
}
