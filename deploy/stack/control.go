package stack

import (
	docker "github.com/fsouza/go-dockerclient"
)

// StartStack start the nginx container
func StartStack(cli *docker.Client, config *ContainerConfig) error {
	return cli.StartContainer(config.ContainerID, &config.HostConfig)
}

// StopStack stop the nginx container
func StopStack(cli *docker.Client, config *ContainerConfig) error {
	return cli.StopContainer(config.ContainerID, 3000)
}
