package dockerclient

import (
	"os"
	"strings"

	docker "github.com/fsouza/go-dockerclient"
	"github.com/sirupsen/logrus"
)

// defaultDockerEndpoint is default docker endpoint
const defaultDockerEndpoint = "unix:///var/run/docker.sock"

// DockerClient stores the main session
type DockerClient struct {
	DockerSession     *docker.Client
	AuthConfiguration docker.AuthConfiguration
}

// GetDockerClientSession get the session
func (d *DockerClient) GetDockerClientSession() *docker.Client {
	if d.DockerSession == nil {
		d.InitializeDockerClient()
	}
	return d.DockerSession
}

// InitializeDockerClient a docker instance
func (d *DockerClient) InitializeDockerClient() {
	client, err := docker.NewClient(defaultDockerEndpoint)
	if err != nil {
		logrus.Error("No connection with docker, ", err)
		os.Exit(1)
	}
	d.AuthConfiguration.Username = "zinnion"
	d.AuthConfiguration.Password = "2Eba0af2"
	d.AuthConfiguration.Email = "mauro@zinnion.com"

	d.DockerSession = client
}

// ListContainers returns a slice containing all existing docker containers on
// the current host (running or otherwise).
func (d *DockerClient) ListContainers() ([]docker.APIContainers, error) {
	return d.DockerSession.ListContainers(docker.ListContainersOptions{All: true})
}

// GetContainerByName get container by name
func (d *DockerClient) GetContainerByName(name string) (docker.APIContainers, error) {

	// list all existing containers
	containers, err := d.ListContainers()
	if err != nil {
		return docker.APIContainers{}, err
	}

	// check all containers to see if one matching our name is present
	for _, c := range containers {
		if strings.TrimPrefix(c.Names[0], "/") == name {
			return c, nil
		}
	}

	return docker.APIContainers{}, nil
}

// PullImage pulls the latest image for the given repotag from a remote
// registry. Credentials (if provided) are used in all requests to private
// registries.
func (d *DockerClient) PullImage(repoTag string) error {

	logrus.WithFields(logrus.Fields{
		"repo_tag": repoTag,
	}).Debug("Pulling latest image from registry")

	// configuration options that get passed to client.PullImage
	repository, tag := docker.ParseRepositoryTag(repoTag)
	pullImageOptions := docker.PullImageOptions{Repository: repository, Tag: tag}

	// parse registry name from repository string
	// var registrystr string
	// parts := strings.Split(repository, "/")
	// if len(parts) <= 1 {
	// 	registrystr = ""
	// }
	// registrystr = parts[0]

	// pull image from registry
	return d.DockerSession.PullImage(pullImageOptions, d.AuthConfiguration)
}

// InspectContainer is a simple proxy function that exposes the method of the
// same name from the instantiated docker client instance.
func (d *DockerClient) InspectContainer(s string) (*docker.Container, error) {
	return d.DockerSession.InspectContainer(s)
}

// InspectImage is a simple proxy function that exposes the method of the same
// name from the instantiated docker client instance.
func (d *DockerClient) InspectImage(s string) (*docker.Image, error) {
	return d.DockerSession.InspectImage(s)
}
