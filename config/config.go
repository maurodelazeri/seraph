package config

import (
	"database/sql"
)

// Cfg config
var Cfg Config

// Config is the overarching object that holds all the information
type Config struct {
	mysqlSession *sql.DB
	Exchanges    []ExchangeConfig `json:"Exchanges"`
}

// ExchangeConfig holds all the information needed for each enabled Exchange.
type ExchangeConfig struct {
	EnabledSymbols []string
}

// LoadConfig loads your configuration file into your configuration object
func (c *Config) LoadConfig() error {

	// c.mysqlSession = mysqlserver.GetMysqlSession()
	// //sql := "SELECT e.name,e.enabled,e.verbose,e.websocket,e.influx_streaming,e.kafka_streaming,e.authenticated_api_support,e.api_key,e.api_secret,e.clientID,e.passphrase,e.basecurrencies,e.assettypes,e.markets,e.taker,e. maker, (SELECT GROUP_CONCAT(pair) FROM exchange_pair p WHERE e.id=p.exchange_id and p.enabled=1) as enabledPairs,(SELECT GROUP_CONCAT(pair_exchange) FROM exchange_pair p WHERE e.id=p.exchange_id and p.enabled=1) as exchangePairs FROM exchange e, exchange_pair p WHERE e.enabled=1 group by e.id"
	// sql := "SELECT e.name,e.enabled,e.verbose,e.websocket,e.kafka_streaming,e.authenticated_api_support,e.api_key,e.api_secret,e.clientID,e.passphrase,e.basecurrencies,e.assettypes,e.markets,e.taker,e. maker, COALESCE((SELECT GROUP_CONCAT(pair) FROM exchange_pair p WHERE e.id=p.exchange_id and p.enabled=1),'0') as enabledPairs, COALESCE((SELECT GROUP_CONCAT(pair_exchange) FROM exchange_pair p WHERE e.id=p.exchange_id and p.enabled=1),'0') as exchangePairs, COALESCE((SELECT GROUP_CONCAT(pair_exchange) FROM exchange_pair p WHERE e.id=p.exchange_id and p.enabled=1 and p.websocket_dedicated=1),'0') as websocketDedicated FROM exchange e, exchange_pair p group by e.id"
	// rows, err := c.mysqlSession.Query(sql)

	// checkErr(err)
	// //websocket_dedicated
	// for rows.Next() {

	// }

	return nil
}

// GetConfig returns a pointer to a confiuration object
func GetConfig() *Config {
	return &Cfg
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
