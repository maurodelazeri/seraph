package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/maurodelazeri/seraph/config"
	"github.com/maurodelazeri/seraph/work"
	"github.com/sirupsen/logrus"
)

// Seraph contains configuration
type Seraph struct {
	config   *config.Config
	shutdown chan bool
}

const banner = `
███████╗███████╗██████╗  █████╗ ██████╗ ██╗  ██╗
██╔════╝██╔════╝██╔══██╗██╔══██╗██╔══██╗██║  ██║
███████╗█████╗  ██████╔╝███████║██████╔╝███████║
╚════██║██╔══╝  ██╔══██╗██╔══██║██╔═══╝ ██╔══██║
███████║███████╗██║  ██║██║  ██║██║     ██║  ██║
╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝  ╚═╝                                            
`

var setup Seraph

func main() {
	sleep := os.Getenv("SLEEP")
	if sleep == "true" {
		logrus.Info("Sleeping 15s to let all services be up...")
		time.Sleep(15 * time.Second)
	}

	HandleInterrupt()

	setup.config = &config.Cfg
	fmt.Println(banner)

	logrus.Infof("Loading config...")

	err := setup.config.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	AdjustGoMaxProcs()

	jobsystem := new(jobsystem.System)
	go jobsystem.Initialize()

	logrus.Infof("Seraph started.\n")

	<-setup.shutdown
	Shutdown()
}

// AdjustGoMaxProcs adjusts the maximum processes that the CPU can handle.
func AdjustGoMaxProcs() {
	logrus.Info("Adjusting seraph runtime performance..")
	maxProcsEnv := os.Getenv("GOMAXPROCS")
	maxProcs := runtime.NumCPU()
	logrus.Info("Number of CPU's detected:", maxProcs)

	if maxProcsEnv != "" {
		logrus.Info("GOMAXPROCS env =", maxProcsEnv)
		env, err := strconv.Atoi(maxProcsEnv)
		if err != nil {
			logrus.Info("Unable to convert GOMAXPROCS to int, using", maxProcs)
		} else {
			maxProcs = env
		}
	}
	if i := runtime.GOMAXPROCS(maxProcs); i != maxProcs {
		log.Fatal("Go Max Procs were not set correctly.")
	}
	logrus.Info("Set GOMAXPROCS to:", maxProcs)
}

// HandleInterrupt monitors and captures the SIGTERM in a new goroutine then
// shuts down seraph
func HandleInterrupt() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		sig := <-c
		logrus.Infof("Captured %v.", sig)
		Shutdown()
	}()
}

// Shutdown correctly shuts down seraph saving configuration files
func Shutdown() {
	logrus.Info("Seraph shutting down..")
	logrus.Info("Exiting.")
	os.Exit(1)
}

/*

export REDIS_FULL_ADDR="redis://127.0.0.1:6379"

export REDIS_DATABASE="0"
export WORKER_NAMESPACE="work"
export WORKER_WEB_UI_ENABLED="true"
export WORKER_WEB_UI_PORT=":6040"
export WORKER_CONCURRENCY="10"
export WORKER_PRIORITY_LIVE="10"
export WORKER_MAX_FAILS_LIVE="1"
export WORKER_PRIORITY_BACKTESTING="10"
export WORKER_MAX_FAILS_BACKTESTING="1"
export LOCAL_INTERFACE="192.168.1.10"

export REDIS_FULL_ADDR="redis://192.168.3.100:6379"

export PORTSLEEP="10s"

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags '-w -s -extldflags "-static"' .

scp winter docker/executer/executer docker/uploader/uploader mauro@64.188.48.130:/var/www/html/
*/
