package brain

import "time"

// Account ...
type Account struct {
	ID        string `json:"id"`
	Balance   string `json:"balance"`
	Hold      string `json:"hold"`
	Available string `json:"available"`
	Currency  string `json:"currency"`
}

// Currency ...
type Currency struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	MinSize string `json:"min_size"`
}

// Order ...
type Order struct {
	Type      string `json:"type"`
	Size      string `json:"size,omitempty"`
	Side      string `json:"side"`
	ProductID string `json:"product_id"`
	ClientOID string `json:"client_oid,omitempty"`
	Stp       string `json:"stp,omitempty"`
	// Limit Order
	Price       string `json:"price,omitempty"`
	TimeInForce string `json:"time_in_force,omitempty"`
	PostOnly    bool   `json:"post_only,omitempty"`
	CancelAfter string `json:"cancel_after,omitempty"`
	// Market Order
	Funds string `json:"funds,omitempty"`
	// Response Fields
	ID            string    `json:"id"`
	Status        string    `json:"status,omitempty"`
	Settled       bool      `json:"settled,omitempty"`
	DoneReason    string    `json:"done_reason,omitempty"`
	CreatedAt     time.Time `json:"created_at,string,omitempty"`
	FillFees      string    `json:"fill_fees,omitempty"`
	FilledSize    string    `json:"filled_size,omitempty"`
	ExecutedValue string    `json:"executed_value,omitempty"`
}

// Fill ...
type Fill struct {
	TradeID   int       `json:"trade_id,int"`
	ProductID string    `json:"product_id"`
	Price     string    `json:"price"`
	Size      string    `json:"size"`
	FillID    string    `json:"order_id"`
	CreatedAt time.Time `json:"created_at,string"`
	Fee       string    `json:"fee"`
	Settled   bool      `json:"settled"`
	Side      string    `json:"side"`
	Liquidity string    `json:"liquidity"`
}
