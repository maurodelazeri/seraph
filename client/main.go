package main

import (
	"log"

	"github.com/gomodule/redigo/redis"
	"github.com/maurodelazeri/seraph-jobsystem"
	"github.com/maurodelazeri/seraph/work"
)

// Make a redis pool
var redisPool = &redis.Pool{
	MaxActive: 5,
	MaxIdle:   5,
	Wait:      true,
	Dial: func() (redis.Conn, error) {
		//return redis.Dial("tcp", "192.168.3.100:6379")
		return redis.Dial("tcp", ":6379")
	},
}

// Make an enqueuer with a particular namespace
var enqueuer = work.NewEnqueuer("work", redisPool)

func main() {
	// Enqueue a job named "live" with the specified parameters.
	var job jobsystem.JobRequest
	// 0 docker entry point file
	// 1 script name to download on antonov
	// 2 (prod only) hadoop strategy location
	// 3 pre command to be executed ex: java -jar
	// 4 name of the file of initialization file
	// 5 extra parameters separated by comma
	job.Entrypoint = []string{"/entrypoint.sh", "init.sh", "/strategies/14310a56d107d8c832ccfb315afb9d40/mauro.tar.gz", "--", "mauro", "param1 param2 etc"}

	job.Parameters = make(map[string]map[string]string)

	job.Parameters["lifetime"] = make(map[string]string)
	job.Parameters["lifetime"]["duration"] = "0"             // seconds 3600 ==> 1h - 0 ==> never
	job.Parameters["lifetime"]["disconnect_police"] = "true" // only execute the bellow rule if this is true
	job.Parameters["lifetime"]["max_disconnect"] = "6"       // 1 pub + 1 req = 2 connections | 6 means 3 disconnect we shutdown, likely the strategy is crashing

	job.Parameters["container"] = make(map[string]string)
	job.Parameters["container"]["image"] = "zinnion/golang:1.10.2-alpine"
	job.Parameters["container"]["container_name"] = "zinnion-golang" // this is unique, if the name already exist we overwide all
	job.Parameters["container"]["hadoop_namenode"] = "192.168.1.11:9000"

	//	job.Parameters["container"]["cpu_shares"] = "0"
	//  job.Parameters["container"]["memory_swap"] = "0"
	//	job.Parameters["container"]["memory_reservation"] = "0"
	//	job.Parameters["container"]["kernel_memory"] = "0"
	//	job.Parameters["container"]["cpu_set"] = "0"

	_, err := enqueuer.Enqueue("live", work.Q{"jobrules": job, "customer_id": "12"})
	if err != nil {
		log.Fatal(err)
	}
}
