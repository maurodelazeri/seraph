package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/maurodelazeri/seraph-client-connector"
	"github.com/sirupsen/logrus"
)

// Message ...
type Message struct {
	Type string `json:"type"`
	Data string `json:"data"`
}

func main() {
	connector := new(connector.Connector)
	go connector.Initialize()

	go func() {
		for {
			if connector.Initialized {
				start := time.Now()
				var data Message
				data.Type = "BuyLimit"
				data.Data = time.Now().String()
				serialized, err := json.Marshal(data)
				if err != nil {
					logrus.Error(err)
					continue
				}
				response, err := connector.MakeRequest(serialized)
				if err != nil {
					logrus.Error(err)
				}
				elapsed := time.Since(start)
				logrus.Info("Response to my request was: ", string(response), " elapsed in: ", elapsed)
				time.Sleep(2 * time.Second)
			}
		}
	}()

	go func() {
		for {
			select {
			case sig := <-connector.FeedMessages:
				fmt.Println("feed push", string(sig))
			default:
			}
		}
	}()

	for {
		select {
		case sig := <-connector.SeraphMessages:
			fmt.Println("received push", string(sig))
		default:
		}
	}
}
