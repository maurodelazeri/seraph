package jobsystem

import (
	"os"
	"strconv"
	"time"

	docker "github.com/fsouza/go-dockerclient"
	"github.com/gocraft/work"
	"github.com/gomodule/redigo/redis"
	"github.com/maurodelazeri/seraph/deploy/docker"
	"github.com/sirupsen/logrus"
)

// System stores the initialization
type System struct {
	WorkerPool *work.WorkerPool
	RedisPool  *redis.Pool
	//Networks   []docker.Network
	Containers []docker.APIContainers
}

// Initialize initializes all necessary stucks
func (s *System) Initialize() {

	// Redis Pool Initialization
	database, err := strconv.Atoi(os.Getenv("REDIS_DATABASE"))
	if err != nil {
		logrus.Error("REDIS_DATABASE must be a number! ", err)
		os.Exit(1)
	}
	s.RedisPool = &redis.Pool{
		MaxActive:   100,
		MaxIdle:     100,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.DialURL(os.Getenv("REDIS_FULL_ADDR"), redis.DialDatabase(database))
		},
		Wait: true,
	}

	// Docker Session
	dockerSession := new(dockerclient.DockerClient)
	client := dockerSession.GetDockerClientSession()

	// networks, err := client.ListNetworks()
	// if err != nil {
	// 	logrus.Error("Cant get the list of networks")
	// 	os.Exit(1)
	// }
	// s.Networks = networks

	containers, err := client.ListContainers(docker.ListContainersOptions{})
	if err != nil {
		logrus.Error("Cant get the list of containers!")
		os.Exit(1)
	}
	s.Containers = containers

	// Job System Pool Initialization
	// Make a new pool. Arguments:
	// Context{} is a struct that will be the context for the request.
	// WORKER_CONCURRENCY is the max concurrency
	// WORKER_NAMESPACE is the Redis namespace
	// redisPool is a Redis pool
	concurrency, err := strconv.ParseUint(os.Getenv("WORKER_CONCURRENCY"), 10, 64)
	if err != nil {
		logrus.Error("WORKER_CONCURRENCY must be a number! ", err)
		os.Exit(1)
	}

	s.WorkerPool = work.NewWorkerPool(Context{}, uint(concurrency), os.Getenv("WORKER_NAMESPACE"), s.RedisPool)

	s.InializeQueues()
}
