package jobsystem

import (
	"fmt"
	"os"
	"os/signal"

	"github.com/gocraft/work/webui"
	"github.com/sirupsen/logrus"
)

// WebUI initializes a rect application to visualize the jobs
func (s *System) WebUI() {

	server := webui.NewServer(os.Getenv("WORKER_NAMESPACE"), s.RedisPool, os.Getenv("WORKER_WEB_UI_PORT"))
	server.Start()
	logrus.Info("Web UI Started Namespace:", os.Getenv("WORKER_NAMESPACE"), " Port", os.Getenv("WORKER_WEB_UI_PORT"))

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)

	<-c
	server.Stop()
	fmt.Println("\nQuitting...")
}
