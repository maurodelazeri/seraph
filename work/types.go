package jobsystem

import (
	"sync"

	"github.com/maurodelazeri/seraph/deploy/disk"
	"github.com/maurodelazeri/seraph/deploy/network"
	mangos "nanomsg.org/go-mangos"
)

// Context is a struct that will be the context for the request
type Context struct {
	customerID        string
	PoliceType        map[string]map[string]string
	PublishSocket     mangos.Socket
	WaitGroup         sync.WaitGroup
	err               error
	Slept             bool
	ReqReqPort        int
	PubSubPort        int
	Verbose           bool
	Disconnections    int
	MaxDisconnections int
	DisconnectPolice  bool
}

// CreateNewContainerResult is model for response container creation result
type CreateNewContainerResult struct {
	DiskConfig    disk.DiskConfig `json:"disk_config"`
	NetworkConfig network.Config  `json:"network_config"`

	//	NginxConfig        nginx.NginxConfig                `json:"nginx_config"`
	//	TransmissionConfig transmissions.TransmissionConfig `json:"transmission_config"`
	//	ResponseMessage    string                           `json:"response_message"`
}

// RequestedJOB stores the job parameters
type RequestedJOB struct {
	Type   string `json:"type"` // live / backtesting
	UserID string `json:"user_id"`
	Image  string `json:"image"`
}

// JobRequest parameters
type JobRequest struct {
	Entrypoint []string                     `json:"entrypoint"`
	Parameters map[string]map[string]string `json:"parameters"`
}

// Message ...
type Message struct {
	Action string `json:"action"`
	Async  bool   `json:"async"`
	Mode   string `json:"mode,omitempty"`
	Feeds  string `json:"data,omitempty"`
}

// type Message struct {
// 	Type          string           `json:"type"`
// 	ProductId     string           `json:"product_id"`
// 	ProductIds    []string         `json:"product_ids"`
// 	TradeId       int              `json:"trade_id,number"`
// 	OrderId       string           `json:"order_id"`
// 	Sequence      int64            `json:"sequence,number"`
// 	MakerOrderId  string           `json:"maker_order_id"`
// 	TakerOrderId  string           `json:"taker_order_id"`
// 	Time          Time             `json:"time,string"`
// 	RemainingSize string           `json:"remaining_size"`
// 	NewSize       string           `json:"new_size"`
// 	OldSize       string           `json:"old_size"`
// 	Size          string           `json:"size"`
// 	Price         string           `json:"price"`
// 	Side          string           `json:"side"`
// 	Reason        string           `json:"reason"`
// 	OrderType     string           `json:"order_type"`
// 	Funds         string           `json:"funds"`
// 	NewFunds      string           `json:"new_funds"`
// 	OldFunds      string           `json:"old_funds"`
// 	Message       string           `json:"message"`
// 	Bids          []SnapshotEntry  `json:"bids,omitempty"`
// 	Asks          []SnapshotEntry  `json:"asks,omitempty"`
// 	Changes       []SnapshotChange `json:"changes,omitempty"`
// 	LastSize      string           `json:"last_size"`
// 	BestBid       string           `json:"best_bid"`
// 	BestAsk       string           `json:"best_ask"`
// 	Channels      []MessageChannel `json:"channels"`
// 	UserId        string           `json:"user_id"`
// 	ProfileId     string           `json:"profile_id"`
// 	LastTradeId   int              `json:"last_trade_id"`
// }
