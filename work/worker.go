package jobsystem

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"time"

	docker "github.com/fsouza/go-dockerclient"
	"github.com/gocraft/work"
	"github.com/maurodelazeri/seraph/deploy/api"
	"github.com/maurodelazeri/seraph/deploy/docker"
	"github.com/maurodelazeri/seraph/deploy/stack"
	"github.com/sirupsen/logrus"
)

// InializeQueues initialize the job queues
func (s *System) InializeQueues() {

	// Add middleware that will be executed for each job
	s.WorkerPool.Middleware((*Context).Log)
	s.WorkerPool.Middleware((*Context).FindCustomer)

	// Map the name of jobs to handler functions
	livePrioriry, err := strconv.ParseUint(os.Getenv("WORKER_PRIORITY_LIVE"), 10, 64)
	if err != nil {
		logrus.Error("WORKER_PRIORITY_LIVE must be a number! ", err)
		os.Exit(1)
	}
	backtestingPrioriry, err := strconv.ParseUint(os.Getenv("WORKER_PRIORITY_BACKTESTING"), 10, 64)
	if err != nil {
		logrus.Error("WORKER_PRIORITY_BACKTESTING must be a number! ", err)
		os.Exit(1)
	}
	liveMaxFails, err := strconv.ParseUint(os.Getenv("WORKER_MAX_FAILS_LIVE"), 10, 64)
	if err != nil {
		logrus.Error("WORKER_MAX_FAILS_LIVE must be a number! ", err)
		os.Exit(1)
	}
	backtestingMaxFails, err := strconv.ParseUint(os.Getenv("WORKER_MAX_FAILS_BACKTESTING"), 10, 64)
	if err != nil {
		logrus.Error("WORKER_MAX_FAILS_BACKTESTING must be a number!")
		os.Exit(1)
	}

	// Customize options:
	s.WorkerPool.JobWithOptions("live", work.JobOptions{Priority: uint(livePrioriry), MaxFails: uint(liveMaxFails)}, (*Context).Live)
	s.WorkerPool.JobWithOptions("backtesting", work.JobOptions{Priority: uint(backtestingPrioriry), MaxFails: uint(backtestingMaxFails)}, (*Context).Backtesting)

	// Start processing jobs
	s.WorkerPool.Start()

	webuidEnabled, err := strconv.ParseBool(os.Getenv("WORKER_WEB_UI_ENABLED"))
	if err != nil {
		logrus.Error("WORKER_WEB_UI_ENABLED must be a boolean! ", err)
		os.Exit(1)
	}

	// Start web ui
	if webuidEnabled {
		go s.WebUI()
	}

	// Wait for a signal to quit:
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt, os.Kill)
	<-signalChan

	// Stop the pool
	s.WorkerPool.Stop()
}

// Log Middleware
func (c *Context) Log(job *work.Job, next work.NextMiddlewareFunc) error {
	fmt.Println("Starting job: ", job.Name)
	return next()
}

// FindCustomer Middleware
func (c *Context) FindCustomer(job *work.Job, next work.NextMiddlewareFunc) error {
	// If there's a customer_id param, set it in the context for future middleware and handlers to use.
	if _, ok := job.Args["customer_id"]; ok {
		c.customerID = job.ArgString("customer_id")
		if err := job.ArgError(); err != nil {
			return err
		}
	}

	return next()
}

// Live Jobs
func (c *Context) Live(job *work.Job) error {

	start := time.Now()
	fmt.Printf("Live: %v\n", time.Now())

	// Extract arguments to execute the job
	jobParams := job.ArgInterface("jobrules")
	if err := job.ArgError(); err != nil {
		logrus.Error("Problem to extract job interface argument")
		return err
	}
	serialized, err := json.Marshal(jobParams)
	if err != nil {
		logrus.Error("Problem to Marshal received parameters")
		return err
	}
	data := JobRequest{}
	err = json.Unmarshal(serialized, &data)
	if err != nil {
		logrus.Error("Problem to Unmarshal received parameters")
		return err
	}

	c.customerID = job.ArgString("customer_id")

	cancelContext, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()

	c.err = errors.New("")

	c.InitializeSocket(cancelContext, cancelFunc)

	resultData := api.CreateNewContainerResult{}

	if container, ok := data.Parameters["container"]; ok {

		if image, ok := container["image"]; ok {
			resultData.StackConfig.ImageName = image
		} else {
			logrus.Error("Cant get config (image)")
			c.err = err
			cancelFunc()
		}

		if name, ok := container["container_name"]; ok {
			resultData.StackConfig.ContainerName = name
		} else {
			logrus.Error("Cant get config (container_name)")
			c.err = err
			cancelFunc()
		}

		if memory, ok := container["memory"]; ok {
			value, err := strconv.ParseInt(memory, 10, 64)
			if err == nil {
				c.err = err
				cancelFunc()
			}
			resultData.StackConfig.Memory = value
		}

		if memorySwap, ok := container["memory_swap"]; ok {
			value, err := strconv.ParseInt(memorySwap, 10, 64)
			if err == nil {
				c.err = err
				cancelFunc()
			}
			resultData.StackConfig.MemorySwap = value
		}

		if memoryReservation, ok := container["memory_reservation"]; ok {
			value, err := strconv.ParseInt(memoryReservation, 10, 64)
			if err == nil {
				c.err = err
				cancelFunc()
			}
			resultData.StackConfig.MemoryReservation = value
		}

		if kernelMemory, ok := container["kernel_memory"]; ok {
			value, err := strconv.ParseInt(kernelMemory, 10, 64)
			if err == nil {
				c.err = err
				cancelFunc()
			}
			resultData.StackConfig.KernelMemory = value
		}

		if CPUShares, ok := container["cpu_shares"]; ok {
			value, err := strconv.ParseInt(CPUShares, 10, 64)
			if err == nil {
				c.err = err
				cancelFunc()
			}
			resultData.StackConfig.CPUShares = value
		}

		if CPUSet, ok := container["cpu_set"]; ok {
			resultData.StackConfig.CPUSet = CPUSet
		}

		if hadoopNamenode, ok := container["hadoop_namenode"]; ok {
			resultData.StackConfig.Env = append(resultData.StackConfig.Env, "HADOOP_NAMENODE="+hadoopNamenode)
		} else {
			logrus.Error("Cant get config (hadoop_namenode)")
			c.err = err
			cancelFunc()
		}

		if os.Getenv("LOCAL_INTERFACE") == "" {
			logrus.Error("Cant get LOCAL_INTERFACE")
			c.err = err
			cancelFunc()
		} else {
			resultData.StackConfig.Env = append(resultData.StackConfig.Env, "SERAPH_REQREP_URL="+os.Getenv("LOCAL_INTERFACE")+":"+strconv.Itoa(c.ReqReqPort))
			resultData.StackConfig.Env = append(resultData.StackConfig.Env, "SERAPH_PUBSUB_URL="+os.Getenv("LOCAL_INTERFACE")+":"+strconv.Itoa(c.PubSubPort))
		}

	} else {
		logrus.Error("Cant get config container")
		c.err = err
		cancelFunc()
	}

	resultData.StackConfig.CustomerID = c.customerID
	resultData.StackConfig.Entrypoint = data.Entrypoint

	// Max of running time, after this we finish this job
	if lifetime, ok := data.Parameters["lifetime"]; ok {
		if timeParam, ok := lifetime["duration"]; ok {
			seconds, err := strconv.Atoi(timeParam)
			if err != nil {
				logrus.Error("Cannot convert duration", err.Error())
				c.err = err
				cancelFunc()
			}
			if seconds > 0 {
				go func() {
					timeChan := time.NewTimer(time.Second * time.Duration(seconds)).C
					for {
						select {
						case <-timeChan:
							cancelFunc()
							fmt.Println("Timer expired")
						}
					}
				}()
			}
		} else {
			logrus.Error("Cant get config (duration)")
			c.err = err
			cancelFunc()
		}

		if disconnectPolice, ok := lifetime["disconnect_police"]; ok {
			disconnect, err := strconv.ParseBool(disconnectPolice)
			if err != nil {
				logrus.Error("Cannot convert disconnect_police", err.Error())
				c.err = err
				cancelFunc()
			}
			c.DisconnectPolice = disconnect
		} else {
			logrus.Error("Cant get config (disconnect_police)")
			c.err = err
			cancelFunc()
		}

		if maxDisconnect, ok := lifetime["max_disconnect"]; ok {
			number, err := strconv.Atoi(maxDisconnect)
			if err != nil {
				logrus.Error("Cannot convert max_disconnect", err.Error())
				c.err = err
				cancelFunc()
			}
			c.MaxDisconnections = number
		} else {
			logrus.Error("Cant get config (max_disconnect)")
			c.err = err
			cancelFunc()
		}

	} else {
		logrus.Error("Cant get config (lifetime)")
		c.err = err
		cancelFunc()
	}

	// Allocate free ports to seraph and container
	//resultData.StackConfig.PortMapping = make(map[int]int)
	//resultData.StackConfig.PortMapping[containerPort] = containerPort
	//	resultData.StackConfig.SeraphAddrPort = seraphPort

	// Docker Session
	dockerSession := new(dockerclient.DockerClient)
	client := dockerSession.GetDockerClientSession()

	// We should not have two containers with under the same name, not likely to happen but need to verified
	container, err := dockerSession.GetContainerByName(resultData.StackConfig.ContainerName)
	if err != nil {
		logrus.Error("Problem check if the container already exist ", err.Error())
		c.err = err
		cancelFunc()
	}
	if container.ID != "" {
		logrus.Info("Duplicated container name found, removing the oldest")

		if container.State == "running" || container.State == "restarting" || container.Status == "paused" {
			err := client.StopContainer(container.ID, 30000)
			if err != nil {
				logrus.Error("Problem stop a container to perform a removing action", err.Error())
				c.err = err
				cancelFunc()
			}
		}

		client.RemoveContainer(docker.RemoveContainerOptions{ID: container.ID})
		if err != nil {
			logrus.Error("Problem to remove an exist container ", err.Error())
			c.err = err
			cancelFunc()
		}
	}

	resultData.CreateNewContainerEndpoint(*client)

	if resultData.StackConfig.ContainerID != "" {

		// Initializing the container
		err = stack.StartStack(client, &resultData.StackConfig)
		if err != nil {
			logrus.Error("Problem to start the container ", err.Error())
			resultData.ResponseMessage = err.Error()
			err := stack.RemoveStackContainer(client, &resultData.StackConfig)
			if err != nil {
				logrus.Error("Problem remove container ", err.Error())
			}
			c.err = errors.New(resultData.ResponseMessage)
			cancelFunc()
		}

		// Get the container Info
		_, err := client.InspectContainer(resultData.StackConfig.ContainerID)
		if err != nil {
			logrus.Error("Problem get the container info: ", err.Error())
			resultData.ResponseMessage = err.Error()
			err := stack.RemoveStackContainer(client, &resultData.StackConfig)
			if err != nil {
				logrus.Error("Problem remove container ", err.Error())
			}
			c.err = errors.New(resultData.ResponseMessage)
			cancelFunc()
		}

		// It means something went wrong
		if resultData.ResponseMessage != "" {
			err := stack.RemoveStackContainer(client, &resultData.StackConfig)
			if err != nil {
				logrus.Error("Problem remove container ", err.Error())
			}
		}

		serialized, _ := json.Marshal(resultData)
		logrus.Info("Container created, connection initializated ", string(serialized))

		c.Start(cancelContext, cancelFunc)

	} else {
		logrus.Error("Problem to create the container: ", resultData.ResponseMessage)
		c.err = errors.New(resultData.ResponseMessage)
		cancelFunc()
	}

	<-cancelContext.Done()
	fmt.Println("The cancel context has been cancelled...")

	logrus.Info("done, time go to ", c.err.Error())

	logrus.Info("Removing container for client id:", c.customerID, " - ", time.Since(start))

	err = stack.RemoveStackContainer(client, &resultData.StackConfig)
	if err != nil {
		logrus.Error("Problem to remove container ", err)
	}

	return nil
}

// Backtesting Jobs
func (c *Context) Backtesting(job *work.Job) error {

	return nil
}

// LiveExternal Jobs
func (c *Context) LiveExternal(job *work.Job) error {

	return nil
}

// BacktestingExternal Jobs
func (c *Context) BacktestingExternal(job *work.Job) error {

	return nil
}
