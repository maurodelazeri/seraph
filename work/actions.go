package jobsystem

import (
	"context"

	"github.com/maurodelazeri/seraph/common"
)

// Buy ...
func (c *Context) Buy(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE Buy")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// BuyLimit ...
func (c *Context) BuyLimit(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE BuyLimit")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// Sell ...
func (c *Context) Sell(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE Sell")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// SellLimit ...
func (c *Context) SellLimit(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE SellLimit")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// Flat ...
func (c *Context) Flat(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE Flat")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// StopMarket ...
func (c *Context) StopMarket(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE StopMarket")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// TrallingStop ...
func (c *Context) TrallingStop(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE TrallingStop")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// TakeProfitLimit ...
func (c *Context) TakeProfitLimit(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE TakeProfitLimit")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// TakeProfitMarket ...
func (c *Context) TakeProfitMarket(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE TakeProfitMarket")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}
