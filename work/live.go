package jobsystem

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/kamilsk/retry"
	"github.com/kamilsk/retry/backoff"
	"github.com/kamilsk/retry/strategy"
	"github.com/sirupsen/logrus"
)

// InitializeSocket ...
func (c *Context) InitializeSocket(ctx context.Context, CancelFunc context.CancelFunc) {

	// Pub/Sub
	c.WaitGroup.Add(1)
	var (
		actionPubSub retry.Action = func(_ uint) error {
			err := c.InitializePubSub(ctx)
			return err
		}
	)
	err := retry.Retry(nil, actionPubSub, strategy.Limit(5), strategy.Backoff(backoff.Fibonacci(5*time.Second)))
	if nil != err {
		log.Fatalf("Failed to listen (actionPubSub) with error %q", err)
	}
	c.WaitGroup.Wait()

	// Req/Rep
	c.WaitGroup.Add(1)
	var (
		actionReqRep retry.Action = func(_ uint) error {
			err := c.InitializeReqReq(ctx)
			return err
		}
	)
	err = retry.Retry(nil, actionReqRep, strategy.Limit(5), strategy.Backoff(backoff.Fibonacci(5*time.Second)))
	if nil != err {
		log.Fatalf("Failed to liste (actionSub) with error %q", err)
	}

	c.WaitGroup.Wait()
}

// Start ...
func (c *Context) Start(ctx context.Context, CancelFunc context.CancelFunc) error {

	go func() {

		fmt.Printf("Current Unix Time: %v\n", time.Now().Unix())

		time.Sleep(60 * time.Hour)

		fmt.Printf("Current Unix Time: %v\n", time.Now().Unix())

		CancelFunc()

	}()

	logrus.Info("Starting the game...")

	return nil
}
