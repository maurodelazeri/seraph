package jobsystem

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/maurodelazeri/seraph/common"
	"github.com/sirupsen/logrus"
	mangos "nanomsg.org/go-mangos"
	"nanomsg.org/go-mangos/protocol/pub"
	"nanomsg.org/go-mangos/protocol/rep"
	"nanomsg.org/go-mangos/transport/ipc"
	"nanomsg.org/go-mangos/transport/tcp"
)

// func (c *Context) subHandler(ctx context.Context, CancelFunc context.CancelFunc) {
// 	for {
// 		select {
// 		case <-ctx.Done():
// 			return
// 		default:
// 			if e := c.PubSocket.Send([]byte("msg mauro")); e != nil {
// 				c.logError("Cannot send pub: %v", e)
// 			}
// 			time.Sleep(2 * time.Second)
// 		}
// 	}
// }

// InitializePubSub ...
func (c *Context) InitializePubSub(ctx context.Context) error {

	seraphPort, err := common.GetFreePort()
	if err != nil {
		logrus.Error("Cant get a free port")
		return err
	}

	var sock mangos.Socket
	if sock, err = pub.NewSocket(); err != nil {
		common.LogError("can't get new pub socket: %s", err)
		return err
	}

	sock.AddTransport(ipc.NewTransport())
	sock.AddTransport(tcp.NewTransport())
	sock.SetPortHook(c.portHook)

	if err = sock.Listen("tcp://" + os.Getenv("LOCAL_INTERFACE") + ":" + strconv.Itoa(seraphPort)); err != nil {
		common.LogError("can't listen on pub socket: %s", err.Error())
		return err
	}

	c.PubSubPort = seraphPort
	c.PublishSocket = sock
	c.WaitGroup.Done()

	return nil
}

// InitializeReqReq ...
func (c *Context) InitializeReqReq(ctx context.Context) error {

	seraphPort, err := common.GetFreePort()
	if err != nil {
		logrus.Error("Cant get a free port")
		return err
	}

	var sock mangos.Socket
	var msg []byte
	if sock, err = rep.NewSocket(); err != nil {
		common.LogError("can't get new rep socket: %s", err)
		return err
	}

	sock.AddTransport(ipc.NewTransport())
	sock.AddTransport(tcp.NewTransport())
	sock.SetPortHook(c.portHook)

	if err = sock.Listen("tcp://" + os.Getenv("LOCAL_INTERFACE") + ":" + strconv.Itoa(seraphPort)); err != nil {
		common.LogError("can't listen on rep socket: %s", err.Error())
		return err
	}

	c.ReqReqPort = seraphPort

	c.WaitGroup.Done()

	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				// Could also use sock.RecvMsg to get header
				msg, err = sock.Recv()
				if err != nil {
					common.LogError("can't read the request: %s", err)
					continue
				}
				logrus.Info("Info I got from sockert is ", msg)
				response, err := c.OnRequest(ctx, msg)
				if err != nil {
					common.LogError("problem to process request (OnRequest): %s", err)
				}

				err = sock.Send(response)
				if err != nil {
					common.LogError("can't send reply: %s", err.Error())
				}
			}
		}
	}()

	return nil
}

func (c *Context) port2Str(port mangos.Port) string {
	local, err := port.GetProp(mangos.PropLocalAddr)
	if err != nil {
		log.Fatalf("failed to obtain local port address: %s", err)
	}
	remote, err := port.GetProp(mangos.PropRemoteAddr)
	if err != nil {
		log.Fatalf("failed to obtain remote port address: %s", err)
	}

	epType := "server"
	if !port.IsServer() {
		epType = "client"
	}
	epState := "open"
	if !port.IsOpen() {
		epState = "closed"
	}
	id := "??"
	if ep, ok := port.(mangos.Endpoint); ok {
		id = fmt.Sprintf("%d", ep.GetID())
	}

	return fmt.Sprintf("%s, local: %s %s~%d, remote: %s~%d#%s",
		epState, epType, local, port.LocalProtocol(),
		remote, port.RemoteProtocol(), id)
}

func (c *Context) portHook(action mangos.PortAction, port mangos.Port) bool {
	if action == mangos.PortActionAdd {
		log.Printf("porthook - adding port: %s", c.port2Str(port))
		if os.Getenv("PORTSLEEP") != "" && !c.Slept {
			var portsleep = 10 * time.Second
			if val, err := time.ParseDuration(os.Getenv("PORTSLEEP")); err == nil {
				portsleep = val
			}
			log.Printf("porthook: sleeping %s to allow for connection drop", portsleep)
			time.Sleep(portsleep)
			c.Slept = true
		}
	} else if action == mangos.PortActionRemove {
		log.Printf("porthook - removing port: %s", c.port2Str(port))
	}
	return true
}

// SockOpts pretty-prints socket options
func (c *Context) SockOpts(sock mangos.Socket) string {
	rdl, err := sock.GetOption(mangos.OptionRecvDeadline)
	if err != nil {
		log.Fatalf("failed to get receive deadline value: %s", err)
	}

	sdl, err := sock.GetOption(mangos.OptionSendDeadline)
	if err != nil {
		log.Fatalf("failed to get send deadline value: %s", err)
	}

	rq, err := sock.GetOption(mangos.OptionReadQLen)
	if err != nil {
		log.Fatalf("failed to get the read-queue length %s", err)
	}

	wq, err := sock.GetOption(mangos.OptionWriteQLen)
	if err != nil {
		log.Fatalf("failed to get the write-queue length %s", err)
	}
	opts := fmt.Sprintf("rx/tx deadline %s/%s, rx/tx qlen %d/%d", rdl, sdl, rq, wq)

	// retry time is the interval between sends, it is only supported on the REQ socket
	ret, err := sock.GetOption(mangos.OptionRetryTime)
	if err == nil {
		opts += fmt.Sprintf(", retry %s", ret)
	} else if err != mangos.ErrBadOption {
		log.Fatalf("failed to get retry interval value: %s", err)
	}

	reconn, err := sock.GetOption(mangos.OptionReconnectTime)
	if err != nil {
		log.Fatalf("failed to get reconnect time value: %s", err)
	}
	reconnMax, err := sock.GetOption(mangos.OptionMaxReconnectTime)
	if err != nil {
		log.Fatalf("failed to get maximum reconnect time value: %s", err)
	}
	return fmt.Sprintf("reconn/max %s/%s, %s", reconn, reconnMax, opts)
}
