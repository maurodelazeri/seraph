package jobsystem

import (
	"context"

	"github.com/maurodelazeri/seraph/common"
	"github.com/sirupsen/logrus"
)

// OnRequest ...
func (c *Context) OnRequest(ctx context.Context, request []byte) ([]byte, error) {
	// EVEN IF WE GOT ERROR WE NEED TO POPULATE THE INTERFACE
	// Status
	// Message
	switch request[0] {
	case 1:
		logrus.Info("YAHHHH im on the server and I got ", request)
		return request, nil
	default:
		processed := "Option not found"
		return []byte(processed), nil
	}

}

// OnPositionChange ...
func (c *Context) OnPositionChange(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE OnPositionChange")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// Ontrade ...
func (c *Context) Ontrade(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE Ontrade")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// OnAccount ...
func (c *Context) OnAccount(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE OnAccount")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// OnConnect ...
func (c *Context) OnConnect(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE OnConnect")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}

// OnDisconnect ...
func (c *Context) OnDisconnect(ctx context.Context) error {
	if e := c.PublishSocket.Send([]byte("DONE OnDisconnect")); e != nil {
		common.LogError("Cannot send pub: %v", e)
	}
	return nil
}
